$(function() {

    // Ячейка сетки, задает точную ширину единичной ячейки
    function Cell(x, width) {
        this.x = x;
        this.width = width;
    }

    // Ряд сетки, задает точную ширину ряда
    function Row(y, height) {
        this.y = y;
        this.height = height;
    }

    // Конечная ячейка (оптимизированая по колспанам и роуспанам)
    function Td(colspan, rowspan, type) {
        this.colspan = colspan || 1;
        this.rowspan = rowspan || 1;
        this.type = type || null;
    }

    // Строит таблицу (вообще говоря, различные функции могли бы быть реализованы разными компонентами, например,
    // можно использовать различные стратегии оптимизации, различные методы отрисовки и т.п.
    // Но так как в JS нет интерфейсов, просто создадим один прототип и все
    function TableBuilder(rectangles) {
        this.rectangles = rectangles;
        this.width;
        this.height;
        this.cells = [];
        this.rows = [];
        this.table = [];
        this.optimizedTable = [];
    }

    // Создает минимальную сетку по координатам граней прямоугольников
    TableBuilder.prototype.buildTable = function() {
        var rectangleCount = this.rectangles.length;

        var cellFacets = [ 0 ];
        var rowFacets = [ 0 ];
        for (var i = 0; i < rectangleCount; i++) {
            // Направляющие ячеек. Так как многие прямоугольники могут иметь одинаковые координаты, проверяем уникальность
            if (cellFacets.indexOf(this.rectangles[i].x1) === -1) {
                cellFacets.push(this.rectangles[i].x1);
            }
            if (cellFacets.indexOf(this.rectangles[i].x2 + 1) === -1) {
                cellFacets.push(this.rectangles[i].x2 + 1);
            }

            // То же самое для рядов
            if (rowFacets.indexOf(this.rectangles[i].y1) === -1) {
                rowFacets.push(this.rectangles[i].y1);
            }
            if (rowFacets.indexOf(this.rectangles[i].y2 + 1) === -1) {
                rowFacets.push(this.rectangles[i].y2 + 1);
            }
        }

        // Отсортируем по возрастанию
        cellFacets.sort(function(a, b) { return a - b; });
        rowFacets.sort(function(a, b) { return a - b; });

        // Теперь превратим в более удобный вид - раньше мы не могли, т.к. не знали длины и ширины
        var cellCount = cellFacets.length - 1;
        for (var ci = 0; ci < cellCount; ci++) {
            this.cells.push(new Cell(cellFacets[ci], cellFacets[ci + 1] - cellFacets[ci]));
        }

        var rowCount = rowFacets.length - 1;
        for (var ri = 0; ri < rowCount; ri++) {
            this.rows.push(new Row(rowFacets[ri], rowFacets[ri + 1] - rowFacets[ri]));
        }

        this.width = this.cells[this.cells.length - 1].x + this.cells[this.cells.length - 1].width;
    }

    // Строит таблицу с колспанами
    TableBuilder.prototype.populateTable = function() {
        var cellCount = this.cells.length;
        var rowCount = this.rows.length;

        var table = [];
        var offset = 0;
        var acc = 0; // Аккумулятор
        for (var ri = 0; ri < rowCount; ri++) {
            var tableRow = {};
            for (var ci = 0; ci < cellCount; ci++) {
                var currentCell = this._isInRectangle(this.rows[ri], this.cells[ci]);

                if (acc >= 0 && currentCell === 1 || acc <= 0 && currentCell === -1) {
                    acc += currentCell;
                } else {
                    tableRow[offset] = new Td(Math.abs(acc), 1, acc > 0 ? 1 : -1);
                    acc = currentCell;
                    offset = ci;
                }
            }

            // Если ряд закончился, запишем и обнулим аккумулятор
            tableRow[offset] = new Td(Math.abs(acc), 1, acc > 0 ? 1 : -1);
            acc = 0;

            table.push(tableRow);
            offset = 0;
        }

        this.table = table;
    }

    // Анализирует таблицу с колспанами и добавляет роуспаны (сооветственно убирая ячейки)
    TableBuilder.prototype.optimizeTable = function() {

        // Проходим с нижнего ряда до верхнего и суммируем роуспаны
        var prevRow = this.table.pop();

        var optimizedTable = [];

        var row = null;
        while (row = this.table.pop()) {

            for (var offset in row) {
                if (prevRow[offset] && row[offset].colspan * row[offset].type === prevRow[offset].colspan * prevRow[offset].type) {
                    row[offset].rowspan += prevRow[offset].rowspan;
                    delete prevRow[offset];
                }
            }

            optimizedTable.push(prevRow);
            prevRow = row;
        }

        optimizedTable.push(prevRow);

        this.optimizedTable = optimizedTable.reverse();
    }

    // Отрисовывает страницу в браузере
    TableBuilder.prototype.renderTable = function() {
        var $table = $('<table>');
        $table.css('width', this.width);
        $table.addClass('rectangle-table');

        // Отрендерим одну строчку для выравнивания ячеек,
        // тем более потом нам будет проблематично это сделать из-за спанов
        var $tr = $('<tr>');
        $tr.css('height', '1px');
        for (var hi = 0; hi < this.cells.length; hi++) {
            var $th = $('<th>');
            $th.css('width', this.cells[hi].width);
            $tr.append($th);
        }
        $table.append($tr);

        // Отрендерим таблицу
        for (var ri = 0; ri < this.rows.length; ri++) {
            var $tr = $('<tr>');
            $tr.css('height', this.rows[ri].height);
            var td = null;
            var row = this.optimizedTable[ri];
            for (var cell in row) {
                var $td = $('<td>');
                $td.attr('colspan', row[cell].colspan);
                $td.attr('rowspan', row[cell].rowspan);
                if (row[cell].type === 1) {
                    $td.addClass('rect');
                }
                $tr.append($td);
            }
            $table.append($tr);
        }

        $('#table').append($table);
    }

    TableBuilder.prototype._isInRectangle = function(row, cell) {
        for (var i = 0; i < this.rectangles.length; i++) {
            var rect = this.rectangles[i];
            if (row.y >= rect.y1 && row.y <= rect.y2 && cell.x >= rect.x1 && cell.x <= rect.x2) {
                return 1;
            }
        }

        return -1;
    }

    $.getJSON('/data4.json', function(data) {

        var tableBuilder = new TableBuilder(data.rectangles);

        // = tableBuilder.make();
        tableBuilder.buildTable();
        tableBuilder.populateTable();
        tableBuilder.optimizeTable();
        tableBuilder.renderTable();
    });
});
